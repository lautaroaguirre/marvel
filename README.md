# Marvel App

To run the project you must create a file in the root directory called .env.development with the following content -> REACT_APP_API_KEY=7dd813eff8664bce73e5884dd59c70b9

### `npm start`

Runs the app in the development mode.

### `npm run build`

Builds the app for production to the `build` folder.

### Additional comments

For this App I used React with Redux, Sass and Material UI (it was my first time using this library, I really enjoyed it, it's easy to use as long as you don't need too much customization). I also used Axios for the HTTP requests to que Marvel API (the api key is stored in the .env files wich are included in the .gitignore file of course).
