import { toast } from 'react-toastify';

import * as types from './types';
import Api from '../lib/api';

const setCharacters = data => ({
  type: types.SET_CHARACTERS,
  payload: data,
});

const setFetchingCharacters = condition => ({
  type: types.SET_FETCHING_CHARACTERS,
  payload: condition,
});

const setCharactersError = () => ({
  type: types.SET_CHARACTERS_ERROR,
});

export const fetchCharacters = (offset = 0) => {
  return async dispatch => {
    dispatch(setFetchingCharacters(true));

    try {
      const { data: { data } } = await Api.get('characters', {
        params: {
          orderBy: 'name',
          limit: 10,
          offset,
        },
      });

      dispatch(setCharacters(data));
    } catch(e) {
      dispatch(setCharactersError());
      let errorMessage = 'Unable to get the characters information';
      if (e.response && e.response.data && e.response.data.message) {
        errorMessage = e.response.data.message;
      }

      toast.error(errorMessage);
    }
  }
};