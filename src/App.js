import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Container } from '@material-ui/core';

import CharactersScreen from './components/CharactersScreen';

import store from './store';

import './App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className='App'>
          <Container fixed>
            <CharactersScreen />
          </Container>
        </div>
      </Provider>
    );
  }
}

export default App;
