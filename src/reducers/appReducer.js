import * as types from '../actions/types';

const INITIAL_STATE = {
  loadingCharacters: false,
  characters: [],
  totalCharacters: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case types.SET_FETCHING_CHARACTERS:
      return { ...state, loadingCharacters: action.payload, characters: [] };
    case types.SET_CHARACTERS:
      return { ...state, characters: action.payload.results, totalCharacters: action.payload.total, loadingCharacters: false };
    case types.SET_CHARACTERS_ERROR:
      return { ...INITIAL_STATE };
    default:
      return state;
  }
};
