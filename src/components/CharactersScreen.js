import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid } from '@material-ui/core';

import HeroesList from './HeroesList';
import HeroProfile from './HeroProfile';

import * as actions from '../actions/actions';

class CharactersScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedHero: null,
      step: 0,
    };
  }

  componentDidMount() {
    this.props.actions.fetchCharacters();
  }

  handleStep = (step) => {
    this.props.actions.fetchCharacters(step * 10);

    this.setState({ step, selectedHero: null });
  }

  onSelectHero = (selectedHero) => {
    this.setState({ selectedHero });
  }

  render() {
    const { loadingCharacters, characters, totalCharacters } = this.props.app;
    const { step, selectedHero } = this.state;

    return (
      <div className="characters-screen">
        <Grid container component="main" spacing={3}>
          <Grid item xs={12} sm={12} md={4}>
            <HeroesList
              totalCharacters={totalCharacters}
              characters={characters}
              step={step}
              handleStep={this.handleStep}
              selectedHero={selectedHero}
              onSelectHero={this.onSelectHero}
              loading={loadingCharacters}
            />
          </Grid>

          <Grid item xs={12} sm={12} md={8}>
            {selectedHero && (
              <HeroProfile
                character={selectedHero}
              />
            )}
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  app: state.app,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CharactersScreen);
