import React, { Component } from 'react';
import { Card, CardContent, CardHeader, Typography, Fab } from '@material-ui/core';
import { Link as LinkIcon } from '@material-ui/icons';

class HeroProfile extends Component {
  render() {
    const { character } = this.props;

    return (
      <div className="hero-profile">
        <h2>Character information</h2>
        <Card elevation={6}>
          <CardHeader
            title={character.name}
            subheader={character.description}
          />

          <img
            alt="Hero"
            className="hero-profile-image"
            src={`${character.thumbnail.path}/landscape_incredible.${character.thumbnail.extension}`}
          />

          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              Useful Links
            </Typography>

            {character.urls.map((url, idx) => (
              <Fab key={idx} className="hero-profile-link" aria-label="Link" variant="extended" onClick={() => window.open(url.url, '_blank')}>
                <LinkIcon />
                &nbsp;{url.type}
              </Fab>
            ))}
          </CardContent>
        </Card>
      </div>
    );
  }
}

export default HeroProfile;
