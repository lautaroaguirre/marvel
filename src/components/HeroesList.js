import React, { Component, Fragment } from 'react';
import { Button, List, ListItem, ListItemAvatar, Divider, ListItemText, Avatar, MobileStepper, Paper, Tooltip, CircularProgress, Grid } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight, ImportContacts as ImportContactsIcon, Tv as TvIcon, Event as EventIcon, Assignment as AssignmentIcon } from '@material-ui/icons';

class HeroesList extends Component {
  renderListItems = (character, idx) => {
    const { characters, selectedHero, onSelectHero } = this.props;

    return (
      <Fragment key={character.id}>
        <ListItem
          alignItems="center"
          button
          selected={selectedHero && character.id === selectedHero.id}
          onClick={() => onSelectHero(character)}
        >
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src={`${character.thumbnail.path}/standard_small.${character.thumbnail.extension}`} />
          </ListItemAvatar>

          <ListItemText
            className="list-text"
            primary={character.name}
            secondary={
              <Fragment>
                {character.comics.available > 0 &&
                  <Tooltip title="This hero appears in a comic" aria-label="This hero appears in a comic">
                    <ImportContactsIcon />
                  </Tooltip>
                }
                {character.series.available > 0 &&
                  <Tooltip title="This hero appears in a series" aria-label="This hero appears in a series">
                    <TvIcon />
                  </Tooltip>
                }
                {character.events.available > 0 &&
                  <Tooltip title="This hero appears in an event" aria-label="This hero appears in an event">
                    <EventIcon />
                  </Tooltip>
                }
                {character.stories.available > 0 &&
                  <Tooltip title="This hero appears in a story" aria-label="This hero appears in a story">
                    <AssignmentIcon />
                  </Tooltip>
                }
              </Fragment>
            }
          />
        </ListItem>
        {idx + 1 !== characters.length && <Divider variant="inset" component="li" />}
      </Fragment>
    );
  }

  render() {
    const { characters, totalCharacters, step, handleStep, loading } = this.props;

    const maxSteps = Math.ceil(totalCharacters / 10);

    return (
      <div className="heroes-list">
        <h2 className="list-title">Characters</h2>
        <Paper elevation={6} >
          <List>
            {loading ? (
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                <CircularProgress />
              </Grid>
            ) : (
              characters.map(this.renderListItems)
            )}
          </List>

          <MobileStepper
            steps={maxSteps}
            position="static"
            variant="text"
            activeStep={step}
            nextButton={
              <Button size="small" onClick={() => handleStep(step + 1)} disabled={(step >= maxSteps - 1) || loading} >
                Next
                <KeyboardArrowRight />
              </Button>
            }
            backButton={
              <Button size="small" onClick={() => handleStep(step - 1)} disabled={(step === 0) || loading} >
                <KeyboardArrowLeft />
                Back
              </Button>
            }
          />
        </Paper>
      </div>
    );
  }
}

export default HeroesList;
