import React from 'react';
import ReactDOM from 'react-dom';
import { toast } from 'react-toastify';

import App from './App';

import 'react-toastify/dist/ReactToastify.min.css';

import './index.scss';

toast.configure({
  autoClose: 10000,
});

ReactDOM.render(<App />, document.getElementById('root'));
