import axios from "axios";

const URL = 'https://gateway.marvel.com/v1/public/';
const apikey = process.env.REACT_APP_API_KEY;

const Api = axios.create({
  baseURL: URL,
  timeout: 20000,
});

// SET THE API KEY IN ALL REQUESTS
Api.interceptors.request.use(config => {
  config.params = { ...config.params, apikey };

  return config;
});


export default Api;
